\section{Background and Motivation}
\label{sec:background}

To provide context for our work, in this section, we first provide a brief overview of Sigover~\cite{sigover} as the first successful signal injection attack in cellular networks and the attack we use to evaluate \T{SigDetect}. We then provide a formalization of channel estimation, which we will use in describing the \T{SigDetect} approach in Section~\ref{sec:design}. Finally we describe wireless channel metrics that have been used in earlier work to detect wireless attacks.

%first introduced SigOver and channel estimation. Then, we analyzed those commonly used approaches for identifying active attackers in wireless communication, their difficulties in detecting signal injection attacks, and the potential of using channel estimation for \T{SigDetect}.
%Then we explain the need for a collaborative endpoint-based detection method. Next, we study and compare RSSI and channel frequency response measurements as potential detection metrics.


\subsection{LTE Basics}
The LTE network infrastructure is designed to provide Internet access to users through the use of end devices called User Equipment (UE), intermediate connectors known as Evolved NodeB (eNodeB) base stations, and a core network called Evolved Packet Core (EPC) which responsible for mobility management. These components work together to establish a reliable connection for users.

\paragraph{ Radio Frequency Signals.}
The UE and eNB communicate with each other using radio frequency (RF) signals that are transmitted and received over an air interface. The frequency domain representation of the RF signal is its spectrum, which shows the distribution of power over the range of frequencies that the signal occupies.
Before being transmitted, RF signals are typically generated in the frequency domain and then modulated to the time domain using OFDM.
In the time domain, the RF signal can be represented by its amplitude or power over time.
Radio frames, which have a duration of 10 milliseconds (ms) and consist of 10 subframes lasting 1 ms each, are used to structure the transmission of the RF signal in the time domain. Each subframe is divided into two equally long slots, each of which contains seven OFDM symbols. This structure is used to efficiently transmit and receive data over the air interface between the UE and eNB in a cellular communication system.


\paragraph{Receive Strength Signal.}
Receive Strength Signal Indicator (RSSI) is a measure of the average power of the signal received by an antenna over a specific measurement bandwidth.  The measurement is based on data from the whole bandwidth and  is calculated using OFDM symbols that contain reference symbols and includes the power of the signal including reference symbols and traffic from the serving cell as well as interference and noise from other sources.

\paragraph{Carrier frequency Offset.}
Carrier frequency offset (CFO) is the difference between the frequency of the carrier signal transmitted by the sender and the frequency at which it is received by the receiver. This difference can be caused by various factors such as the Doppler effect due to the relative movement between the transmitter and receiver, the oscillator's frequency drift over time, or the frequency error of the oscillator used in the transmitter or receiver.

\paragraph{Channel Response.}
\label{Channel Information}

The communication channel can cause various distortions in the shape and strength of a received signal, such as fading, scattering, power decay, and noise. To correct for these distortions, the receiver must estimate the overall effect that the channel has on the signal as it is transmitted, known as the channel response. This can be represented in both the time and frequency domains, using the channel impulse response (CIR) and the frequency response (CFR), respectively. The two channel responses can be estimated by the following equations:
\begin{align}
\label{1}
y &= h \ast x  + w\\
Y &= H X + W
\end{align}

where $y$ and its Fourier transform $Y$, $x$ and its Fourier transform $X$, are the received and transmitted reference signals (RS) which are designed to be known by both UE and eNB for the purpose of channel estimation in the time and frequency domains. $w$ and $W$ represent white Gaussian noise, and $h$ and $H$ represent the CIR and CFR, respectively.

\iffalse
Since signal injection attacks do not contaminate RS, when malicious signals overlay legitimate signals, the superimposed channel can be written as Equation \ref{2}, 
where $H$ and $H_a$ are the channel frequency responses of the original cell and the attacker. $y'$ and $Y'$ are the composite RS.
\begin{align}
\label{2}
\begin{split}
y' &= h \ast x  + h_a\ast x + w\\
Y' &= H X + H_a X + W
\end{split}
\end{align}
\fi

\begin{figure*}[htb]
\centering
\includegraphics[width=0.8\linewidth]{fig/overview.pdf}
\caption{SigDetect Overview: Affected endpoints are reporting a detected signal injection attack.}
\label{fig.sigdetect}
\end{figure*}


\subsection{Signal Overshadowing Attack}

%SigOver, the first published successful signal injection attack associated with cellular networks, directly manipulates LTE broadcast messages by overlaying the original broadcasting message with a counterfeited one to achieve a variety of~\cite{sigover}.\footnote{SigOver is applied to the LTE-Frequency Division Duplex (FDD) mode, which is widely used by mobile operators across te globe~\cite{global2018evolution}. While we evaluate \T{SigDetect} in an FDD system in order to detect SigOver, we expect similar performance for TDD systems.} The adversary in this attack synchronizes in time and frequency to a legitimate base station and eavesdrops on downlink broadcast messages in order to forge a malicious subframe under the legitimate message framework.



To perform a signal overshadowing attack with a high success rate, an attacker follows a series of steps including synchronizing tightly with a cell in the network to obtain its time clock, crafting malicious subframes using Information Elements (IEs), and injecting them into the network with an at least 3 dB higher transmission power than legitimate subframes. These malicious subframes, when received and decoded by victim devices, will exhibit the intended behavior of the attacker. The target of the attack can be a group of victims through the injection of malicious subframes into System Information Block (SIB) subframes, or a specific individual through the injection of subframes into paging subframes. Many attack scenarios can be implemented using signal overshadowing, and the authors of this work have shared the source code for multiple such scenarios. In the development of a detection method, the focus is on the type of attack being carried out rather than the specific scenario being used, so the paging with IMSI scenario was chosen as a model.

Paging with IMSI attack scenario is a type of signal overshadowing attack that exploits the rules of paging mobility management in a wireless communication network. In this scenario, the attacker injects malicious paging messages into the network, using either the permanent identity (IMSI) or temporary identity (S-TMSI) of a specific target UE (User Equipment). Paging messages are typically used to notify a particular UE of the arrival of downlink data and to establish a communication connection. Normally, after a UE camps on a cell, it obtains an S-TMSI to replace its more private IMSI identifier. However, if there is a network failure or an S-TMSI allocation failure, a paging message with IMSI may be transmitted. The SigOver attack can exploit this by injecting paging messages with the target's IMSI repetitively, causing the UE to continuously reattach to the cell and potentially causing a Denial of Service (DoS) attack.


%Fig~\ref{attack_model} illustrates the signal overshadowing attack. Specifically, time is divided into 1-ms subframe units. Broadcasting messages carrying cell configuration and other downlink shared information is periodically allocated to specific subframes. A SigOver attacker synchronizes with a cell to get the cell's time clock, delicately crafts malicious subframes, and injects the fake subframes with a slightly higher transmission power (about 3 dB from capture effect theory hl{capture effect}) to overshadow the legitimate subframe. Such manipulated, legitimate-looking messages can be correctly decoded by the victim UEs, yielding the behavior intended by the adversary.

%The adversary crafts the subframes by filling in particular Information Elements (IEs) with malicious information to accomplish diverse attack goals for a one or more UEs within the attack range. Table~\ref{attacks} shows the variety of potential attacks. Five attack scenarios can be divided into two categories according to the target. The attack aims at a group of victims when the injection is on the System Information Block (SIB) subframes. When SigOver is on paging subframes, a specific individual is targeted. For more details about the implementation of each attack scenario refer to~\cite{sigover}. The authors of this work were willing to share the source code for the overshadowing of paging messages with IMSI, so we used that as a model for our detection method.

%Paging messages, a typical broadcasting instance, notifying a particular UE of the downlink data arrival to establish a communication connection, attach either a permanent identity (IMSI) or a temporary identity (S-TMSI) of the UE assigned by the cell. Normally, after a UE camps on a cell, it obtains its S-TMSI to replace the more private IMSI identifier. Only when there is a network failure or an S-TMSI allocating failure will a paging message with IMSI be transmitted. Thus, SigOver can keep a UE reattaching a cell to achieve a Denial of Service (DoS) attack by injecting paging messages with the target's IMSI, if known, repetitively.


\subsection{Motivation}
\label{motive}
The concept of "signal overshadowing" involves covering the original signals with the attack signals, resembling a spoofing attack, with the difference being that the malicious signal are the combination of both the attack signals and the original signals. Typically, solutions for detecting spoofing rely on using channel response information.\hl{citations here} Drawing on these approaches, we were motivated to investigate the impact of signal overshadowing on the channel.

\begin{figure}[htpb]
     \centering
     \includegraphics[width=0.45\textwidth]{fig/observation.png}
     \caption{}
     \label{observation}
\end{figure}


Figure \ref{observation} presents the results of an outdoor Line-of-Sight (LOS) replication of the SigOver attack using three fixed end-point USRP B210s on Powder Platform~\cite{}.  Rather than relying on srsRAN's method of locating the peak of the correlation of the sync signal for synchronization, Powder's end-points are linked to a synchronization system known as White Rabbit. This system provides a 10 MHz and PPS reference to all connected devices, leading to a sub-nanosecond synchronization between them. This controlled environment enables more meticulous observation and experimentation. The figure displays the original frequency responses of a legitimate channel and an attacked channel, as well as how the frequency response of the legitimate channel changes when subjected to various timing offsets during the attack. the attack can go undetected by the UE. However, as the timing offset increases, specific patterned fluctuations appear in the frequency response amplitudeHence, there is a need for a simple detection and localization method that leverages the available channel information with minimal computational demands through a crowd-sourcing approach.This approach involves exchanging lightweight information and keys for efficient detection and localization while minimizing payload strength.


\iffalse
SigOver surpasses other spoofing attacks, such as the FBS or MITM, in power efficiency, stealthiness, and sustainability. It targets at the subframe level, accepts lower transmission power, and avoids cutting off the victim's connection to the cell while achieving a comparable attack success rate as the FBS and MITM. Seems impeccable; nonetheless, SigOver demands a relatively high precision in timing and frequency synchronization, which inspires us to 


By utilizing the policy loopholes and counterfeiting different broadcasting messages, SigOver produces various types of long-duration, sustainable attack goals as listed in Table \ref{sigOver}, continuously affecting individuals and groups without being aware.  In this section, we provide some necessary background to the SigOver attack.  First, we introduce two broadcasting messages and the corresponding Information Elements (IEs) that are assailable.  Then, we give a brief explanation of the attack model.

\subsection{Cell Search and Synchronization}
When a UE is turned on, it measures the RSSI over a set of candidate frequency channels and filters out candidates with low RSSI. After that, it obtains the primary synchronization signal (PSS) location to synchronize with the candidate cell in the time dimension. And then, the carrier frequency offset is estimated and compensated to synchronize in the frequency domain. The UE detects the radio frame timing, Physical Layer Cell ID, and cyclic prefix length from the secondary synchronization signal (SSS). Afterward, the UE reads the Master Information Block (MIB) and System information blocks (SIB) to check the Public Land Mobile Network (PLMN) identifier, camps on a cell, and reads other SIBs on that cell.

We consider a cellular system with a legitimate BS, an attacker and a MS, 
shown in Fig.\ref{system_model}.

% Figure: attackmodel
\begin{figure}[ht]
\centering
\includegraphics[width=5cm]{fig/system_model.pdf}
\caption{Attack a target UE by impersonating a legitimate BS  }
\label{system_model}
\end{figure}

The attacker is supposed to transmit its malicious data stream $x_2$, impersonating a legitimate one $x_1$, 
to the MS at the same time with the same carrier frequencies as the BS. 
In other words, we assume the malicious signals is a synchronized signa
We use $h_i$ and $H_i$ to represent the downlink channels before and after FFT. 
Subscript $1$ is from the BS to the MS and subscript $2$ from the attacker. 
And the channel is estimated by the reference signals (pilots).


\subsection{Targeted messages}
\subsubsection{System Information Block (SIB)}
Among all the SIB, SIB1 and SIB2 are essential for UE to camp on a specific cell. A moving UE retrieves the Tracking Area Code (TAC) in the SIB1 of the cell and validates it with its TAI list to identify a nearby new base station to hand over for better service. Repeating fake TAC not on the TAI list triggers UE sending TAU requests and thus traps UE in the TAU process forever. The BarringFactor parameter in SIB2 limits the number of UE that can access the network. SigOver attacker can exploit this parameter to block a group of UE by setting it to 0.

\subsubsection{Paging}
Another vulnerable shared message is paging which broadcasts downlink arrival, emergency alert, or system information change in a time window with a fixed cycle.  Following IEs in the paging message can be leveraged to achieve different attacks. 

\begin{itemize}
\item Indentity: Each UE has a permanent identity, International Mobile Subscriber Identity (IMSI), and a temporary identity, S-TMS, assigned by the cell.  Paging with IMSI usually implies a network or an S-TMSI  allocating failure; by injecting paging messages with IMSI repetitively, SigOver can keep a UE reattaching a cell to achieve  (Denial of Service) DoS attack.  In addition,  SigOver can trace a victim's coarse-grained geolocation by forcing the UE to set up a connection to the Network with its S-TMSI and eavesdropping on the Connection setup messages from the legitimate cell.  
\item Core-network domain: SigOver can downgrade the victim UEs to 3G Network by replacing Packet Switched (PS) Service indicates a data transfer or an incoming SMS with Circuit Switched (CS) notifies a fall-back speech call service in a paging message
\item ETWS:  SigOver can false alarm emergency alerts via Earthquake and Tsunami Warning System (ETWS) and Commercial Mobile Alert System (CMAS) in a paging message
\item System information modification: SigOver can notify victims to read modified SIB1 by setting the field to be true in a paging message.
\end{itemize}


SigOver can be used to accomplish diverse attack goals by crafting particular Information Elements (IEs) in corresponding subframes, as shown in Table \ref{SigOver}.  One target message is System Information Blocks.  Before handing over to a new cell, a UE needs to indentify the cell by retrieving the Tracking Area Code (TAC) in the SIB1 and validating it with its TAI list.  Repeating fake TAC that is not on the TAI list triggers UE sending TAU requests and thus traps UE in the TAU process forever.  SigOver attacker can exploit the BarringFactor parameter in SIB2, limiting the number of UE that can access the Network to block a group of UE by setting it to 0.  Another vulnerable shared message is paging which broadcasts downlink arrival, emergency alert, or system information change in a time window with a fixed cycle.  Each UE has a permanent identity, International Mobile Subscriber Identity (IMSI), and a temporary identity, S-TMS, assigned by the cell.  Paging with IMSI usually implies a network or an S-TMSI allocating failure; by injecting paging messages with IMSI repetitively, SigOver can keep a UE reattaching a cell to achieve (Denial of Service) DoS attack.  In addition, SigOver can trace a victim's coarse-grained geolocation by forcing the UE to set up a connection to the Network with its S-TMSI and eavesdropping on the Connection setup messages from the legitimate cell.  SigOver can downgrade the victim UEs to 3G Network by replacing Packet Switched (PS) Service indicates a data transfer or an incoming SMS with Circuit Switched (CS) notifies a fall-back speech call service in a paging message.  SigOver can false alarm emergency alerts via Earthquake and Tsunami Warning System (ETWS) and Commercial Mobile Alert System (CMAS) in a paging message. SigOver can notify victims to read modified SIB1 by setting the field true in a paging message.
\fi
