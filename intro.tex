\section{Introduction}
\label{intro}

Broadcast messages transmitted between base stations and endpoints without authentication are vulnerable to attack in 4G and 5G networks. Adversaries can exploit this vulnerability by using fake base stations with higher transmission power to lure devices into connecting and mounting sophisticated attacks, as victims tend to prefer better connection quality and are unable to verify the source of system information. However, the high-power signal source also makes these attacks detectable and the attacker traceable. A recently proposed signal spoofing attack called SigOver ~\cite{sigover} and its variations, such as AdaptOver and LTrack ~\cite{erni2021adaptover,kotuliak2022ltrack}, \hl{as well as similar attacks targeting IoT data plane messages}, can launch various attacks (e.g., denial of service (DoS), network downgrade, and coarse-grained tracking) using much lower power than other attacks by overshadowing a single broadcast subframe rather than trying to overpower most legitimate transmissions consistently. To avoid confusion, we will refer to all of these attacks as overshadow attacks throughout the rest of the paper. These stealthier attacks significantly reduce hardware requirements and make them imperceptible and untraceable.

\begin{figure}[t]
\centering
\includegraphics[width=8cm]{fig/attack_model.png}
\caption{Signal overshadowing attack: the adversary synchronizes with the cell and then generates a crafted subframe (shown in orange) and injects it at the appropriate time. This allows the adversary to interfere with the normal operation of the cell.}
\label{attack_model}
\end{figure}

There have been numerous studies on detecting spoofing attacks, but they do not specifically target overshow attacks, which are directed at cellular signals. Most of these studies use the strength of the reference signal (RSSI) to detect spoofing, but this method is not well-suited for cellular networks because the synthetic signal from overshow attacks is typically weak compared to the natural fluctuations in RSSI. As a result, these methods may not be effective against overshow attacks. Other techniques that do not rely on RSSI have been proposed, such as methods that detect sudden changes in channel information such as the channel frequency response (CFR), channel impulse response (CIR), or carrier frequency offset (CFO). While some of these methods have demonstrated effectiveness in certain scenarios, their ability to detect overshow attacks in cellular networks has not been thoroughly evaluated.
%Except for the CFR cross-correlation-based method mentioned in ~\cite{sigover} has been proven effective for LOS scenarios but not for NLOS, the effectiveness of other methods on sigover has yet to be verified. 

Alongside the detection, finding the exact geographical location of the detected spoofing attack is also a significant matter when it comes to taking technical or legal actions.
Although several works exist in the literature regarding how to make positioning in mobile networks, none of them deal with locating the signal overshadowing attack specifically ~\cite{}. Most time-of-arrival (ToA)-based approaches are designed to handle single-source radio signals rather than multiple concurrent signals from multiple transmitters. One study (cited in ~\cite{pinpoint}) addresses the problem of localizing interference radios by using the Cyclic Cross-Correlation Function (CCCF) on received signals with suspected interference signals to obtain the line-of-sight angle-of-arrival (AoA) of the interference radios at the first CCCF peaks on each antenna. However, this approach may not be effective when interference signals are closely correlated with legitimate signals, such as in the case of overshadow attacks, where the attack signals are generated from similar messages with only a few modified bits.


%Ericsson noticed the need for Communication service providers to detect impersonation attacks that cause service disruption, loss of privacy, and brand damage and adopted a more practical, precise, and timely detection FBS detection framework ~\cite{nakarmi2018detecting} (but not localization). However, stealthier attacks like SigOver, capable of mounting various attacks without pretending to be an entirely legitimate base station, can evade detection by this framework. 

Our paper introduces SigDetect, a system that is able to effectively detect overshadow attacks with a higher accuracy compared to RSSI-based methods, even in situations where the detectors are moving and the attacker is not in a direct line of sight. In addition, SigDetect can moderately accurately locate the attacker when legitimate and malicious signals are being transmitted concurrently, without the need for expensive calibration or additional RF hardware. This makes it a unique and cost-effective solution for detecting and locating overshadow attacks.


SigDetect's main contribution is a novel algorithm that can finely estimate the time delay of the malicious radio attack signal compared to the legitimate signal at all detectors in a line-of-sight scenario using a specific characteristic of the channel frequency response. This enables the detection of overshadow attack. Once an attack is detected, a few detectors estimate and share the attacker's time delay, and SigDetect runs a hyperbola-based algorithm to calculate the attacker's exact location. The challenge of estimating the attacker's delay is twofold. First, commercial cellular networks and IoT devices with narrow bandwidths and low sampling rates may not accurately capture the subtle time delay due to low resolution in time. Second, determining the arrival time of the attacker's line-of-sight component using only information from superimposed signals is non-trivial. Therefore, SigDetect must disentangle the attack and legitimate signals with limited information in order to address the time delay estimation problem.

Our key insight is that the attacker and the cell must be closely synchronized for the attacks to be successful; however, , in practice, the synchronization is not perfect, resulting in a delay between the attack and legitimate signals on detectors in the vicinity of the attacker. We designed our approach based on the channel frequency response that can exploit these relative delays. Specifically, the delay between the attack and legitimate signals results in a quasi-periodic signal in the channel frequency response. Reducing the attack's time offset can improve the attack success rate while making the periodicity more prominent, which, in turn, helps SigDetect's detection accuracy. Conversely, increasing the offset decreases the detection's effectiveness, but the attack's success is not guaranteed.Thus, we first use a binary classifier to detect such anomalous quasi-periodic features in the channel measurements associated with overshadow attacks. Next, we apply a Discrete Fourier Transform (DFT) to the channel frequency response to extract the period, which is then used to estimate the time delay between the attack signal and the legitimate signal. To overcome the resolution limitation, we increase the number of FFT bins and apply Gaussian spectrum interpolation~\cite{} to the DFT magnitude spectrum of the channel frequency sequence.


SigDetect can convert periodic information from the distorted channel information on the device near the attacker to delay of attack signal. As far as we know, no prior localization technique has been able to isolate and compute the time delay of overshadow attack signals. We implement SigDetect using standard USRP and evaluate \hl{SigDetect using testbed experiments in an indoor environment for LOS and NLOS detection and an outdoor environment for LOS localization.} We compare SigDetect against the state-of-the-art RSSI-based approach. We find that: 

\begin{itemize}
  \item SigDetect is more accurate in either LOS or NLOS environments. SigDetect achieves an average Area Under Curve (AUC) score of 0.965 with a group of six detectors in LOS scenario, which is 0.419 higher than the RSSI and xx higher than the CSI-based method. And Its average  AUC score reaches xx in the NLOS scenario, while the RSSI and CSI-based approaches achieve AUC scores of xx and xx, respectively.
  \item SigDetect maintains good detection accuracy even in mobility deployments. SigDetect can keep the AUC score around xx and xx in LOS and  NLOS mobility scenarios, which is superior than xx and xx using the RSSI-baed method, and also better than  xx and xx using the CSI-based method.
  \item SigDetect achieves a moderate LOS localization error of xx meters, which is around xx times lower compared to the RSSI.
\end{itemize}

Finally, we discuss the possibility of SigOver in 5G networks showing the need for \T{SigDetect} and \T{SigDetect}'s potential to be a crowd-sourced solution given the trend for manufacturers to open the interface and provide access to more physical layer channel information. 



\iffalse
According to Cisco Annual Internet Report (2018–2023)~\cite{cisco_report_2020}, over 70~\% of the global population will have mobile connectivity by 2023, and global mobile devices will grow to 13.1~billion, among which 1.4~billion will be 5G capable. 
Further, governments across the globe are exploring the use of 5G technologies in various use cases~\cite{operatethrough5g,australiamilitary5g,eudefense5g}.
%Such popularity also brings concern to 4G/5G security. In 4G, plain texts are exchanged between UEs and networks without encryption before secure communication protected by AES has been successfully established, leaving space for eavesdropping and illegal access to sensitive information. The vulnerability remains in 5G (see Section \ref{5G}).
These developments suggest a continued growing interest in the security of cellular networks. However, various unaddressed attack surfaces remain in these systems. In 4G, e.g., unencrypted plain text messages are exchanged between user equipment (UE) devices and networks before secure communication protected by Advanced Encryption Standard (AES) has been established, leaving opportunities for eavesdropping on sensitive information. While the security of 5G networks is significantly improved, \hl{5G broadcast messages are still unencrypted/unsigned enabling some attack vectors.} (See Section~\ref{5G}).
\fi
%Broadcasting providing cells' system information for UEs to camp is designed unencrypted for serving all devices in an area - a natural target for spoofing and tampering. Fake Base Station, Man-in-the-Middle (MitM) \cite{rupprecht2019breaking}, and International Mobile Subscriber Identity (IMSI) Catcher exploiting the Cell Selection Criteria, i.e., UE camps on the cell with the best radio condition, masquerade as legitimate cell towers with high transmission power to lure victims into connecting. It arouses concerns for users' privacy as such attacks allow attackers to obtain the most private and vital data - IMSI in LTE or Subscription Permanent Identity (SUPI) in 5G and derive more other attacks. But high returns always come with high risks; the signal source discloses the attackers' location. To lower the detection risk, \T{SigOver} \cite{\T{SigOver}} displaces a piece of broadcasting information using limited transmission power to accomplish all kinds of attacks, e.g., DoS, network downgrade, and coarse-grained tracking. And its persistency, operability, and stealthiness compound the threat.

\iffalse
Impersonation attacks like Fake Base Station (FBS), Man-in-the-Middle (MitM)~\cite{rupprecht2019breaking}, and International Mobile Subscriber Identity (IMSI) Catchers exploit the Cell Selection Criteria. I.e., since the UE will attach to the cell with the best radio condition, \hl{attackers masquerade as legitimate cell towers and use high transmission power to lure victims into connecting. These attacks are capable of harvesting one of the most private and vital pieces of information in cellular networks - the international mobile subscriber identity (IMSI) in LTE or subscription permanent identity (SUPI) in 5G} - and use it to mount other attacks. But high-power signal source makes these attacks detectable and can be used to localize the attacker. More recent stealthy signal injection attacks, i.e., SigOver~\cite{sigover} and efforts that built on it~\cite{erni2021adaptover,kotuliak2022ltrack}, ``overshadows'' a single broadcast subframe rather than trying to consistently overpower most legitimate transmissions, and is able to mount various attacks (e.g., denial of service (DoS), network downgrade, and coarse-grained tracking) using much lower power than other attacks. These more stealthy signal injection attacks significantly reduce the hardware requirements for the attack, and also make it much harder to detect.
\fi
%Broadcast messages that provide the system information required for UEs to attach are transmitted unencrypted to any device within range of the cell - a natural target for spoofing and tampering. \hl{maybe should mention how paging messages are vulnerable somewhere around here}

%Detecting signal injection attacks is not new, but previous research may be ineffective in detecting \T{SigOver}, an improved version of signal injection attacks. In many research studies, the Reference Signal Strength Indicator (RSSI) is easy to access and always the first choice for spoofing attack detection. Though the indicator suffers from its unreliable,  time-varying nature \cite{zhou2004impact}. Specifically, a study shows that in a stable indoor environment, the variations of RSSI from an immobile receiver can be up to 5 dB in a minute \cite{wu2012fila}. Moreover, Section \ref{RSSI} shows the payload size of the message and environment account for the variation of RSSI. In certain research cases, the variation is tolerant or can be compensated by other indicators \cite{wu2020blueshield} while for \T{SigOver} detection, it inevitably results in unsatisfactory performance. Since \T{SigOver} requires only 3 dB higher power than the legitimate signal, RSSI's natural fluctuations conceal this minor increase in the received signal strength, which offers shelter to the \T{SigOver} attack. The weak transmission power adds to the difficulty for a victim being aware of the attack, not to say those nearby UEs that identify the attacker as noise. Thus, we assess that the RSSI-based method does not suit the \T{SigOver} defense case in any direction.

\iffalse
To the best of our knowledge, other than the channel estimation cross-correlation approach suggested in the original SigOver paper\footnote{The SigOver paper showed how changes to the normalized cross-correlation of the channel estimation magnitude could indicate the presense of an attack. However, they also suggest the robustness of this approach would be problematic, especially in none-line-of-sight (NLOS) scenarios.}, and Successive Interference Cancellation (SIC) approach used for the SigUnder - an injection attack built on SigOver, no other detection methods have been proposed for stealthy signal injection attacks in cellular network scenarios.\hl{Note: channel estimation based approaches have been well studied for pilot spoofing attack in cellular networks, the difference between pilot spoofing and signal injection is that pilot spoofing contaminates the reference signals to prevent UEs decode message with correct channel estimation. The cross-correlation approach or other channel estimation based approaches heavily rely on the channel difference between attackers and legitimate base station, and thus, are less effective in None Line of Sight (NLOS) scenario. SIC-based approach need to recover message from noise and then confirm the attack, which is impractical.} Detecting injection attacks have been studied in different wireless contexts, i.e., WiFI and Bluetooth~\cite{}. These earlier efforts used receive signal strength indicator (RSSI)~\cite{}, carrier frequency offset (CFO)~\cite{} based, and also channel estimation-based approaches to detect attacks. These methods are, however, ineffective for detecting stealthy signal injection attacks, like SigOver, in a cellular network scenario. First, RSSI measurements can vary significantly in the absence of any attack~\cite{zhou2004impact,wu2012fila}, thus reducing its utility as an attack detector. Moreover, in Section~\ref{RSSI}, we show that the time-varying nature of resource scheduling in cellular networks, as well as changes in the environment of the link, cause significant variations in RSSI. Second, CFO-based detection approaches attempt to detect attacks by comparing CFO changes, from the perspective of a device under attack, between a legitimate base station and an attacker. However, for stealthy signal injection attacks to be successful, the attacker and the cellular base station needs to be tightly synchronized for the attacks to succeed, which, therefor renders CFO based attack detection approaches ineffective.
\fi 

\iffalse
In \T{SigDetect}, our key insight is to make use of channel estimation metrics that changes in the presence of an attack and at the same time is robust to the specific characteristics in a cellular environment. \hl{Channel itself changes after an attack and reflects on channel estimation, but further,} we observe that in the presence of an attack the attacked endpoint will observe the sum of two waves, one from the legitimate base station and one from the attacker. The sum of these two waves result in a quasiperiodic signal in the channel frequency response. \T{SigDetect} uses this quasiperiodic signal to detect the pressence of an attack. \hl{From our study, less timing offset longer quasiperiod. The attacker and the cellular base station needs to be tightly synchronized for the attacks to succeed, which, therefore helps the SigDetect identify the attacks from increased quasiperiod.}
\fi

\iffalse
Using features from this quasiperiodic signal, \T{SigDetect} uses a binary classifier targeted at detecting anomalous channel measurements. It is impractical to train the classifier on channel measurements under attack, since, assuming we deploy detection equipment before or outside the attack range, only normal samples are available. Thus, a binary classifier turns into a one-class classification for identifying outliers, a problem for Novelty Detection. While our evaluation of \T{SigDetect} is focused on its performance detecting SigOver, the general approach is applicable to any physical layer attack where a malicious transmitter is (a) located at a different location than the transmitter it wishes to impersonate/overshadow and (b) potentially imperfectly synchronized in time and/or frequency with the legitimate transmitter.


Another critical difference between \T{SigDetect} and earlier efforts to detect (non-cellular) signal injection attacks relates to the \textit{operational context} of the detection. Earlier efforts focused exclusively on detecting attacks from the point of view of the victim. Such detection is of course useful, i.e., the user of the device can be notified the device is under attack. However, once an attack is detected, the victim device is effectively in a denial-of-service condition, and as such cannot report the attack to the network (or authorities capable of acting on the information). \T{SigDetect}'s collaborative approach addresses this limitation. Specifically, endpoints in the vicinity of the affected node collaborate to perform attack detection. In this manner detected attacks can be reported for further investigation. The \T{SigDetect} model is depicted in Figure~\ref{fig.sigdetect}. 
\fi


\iffalse
RSSI-based methods have been used for detecting a wide variety of wireless attacks, impersonation attacks (see Section~\ref{reference}) and spoofing attacks (similar to signal injection attacks forging identities instead). However, these methods may be ineffective for 

detecting stealthy signal injection attacks, like SigOver, which use a more targeted and concealable approach than, e.g., FBS attacks. RSSI measurements can vary significantly in the absence of any attack~\cite{zhou2004impact}, reducing detector performance. Specifically, a study shows that in a stable indoor environment the RSSI measurements made at an immobile receiver can vary by up to 5 dB in a minute~\cite{wu2012fila}. Moreover, in Section~\ref{RSSI}, we show that the time-varying nature of resource scheduling in cellular networks, as well as changes in the environment of the link, cause significant variations in RSSI. Hence, RSSI is not ideal for detecting the SigOver attack, since SigOver requires only 3 dB higher power than the legitimate signal and RSSI's natural fluctuations may conceal the minor change caused by the attack.
\fi

%Aside from RSS, we can monitor abnormal channel changes to detect \T{SigOver}. Previous studies (see Section \ref{reference}) introduced many channel features to capture spoofing attacks. However, the attack's impact on the channel characteristics in these researches is commonly analyzed from the victim's perspective. We notice that the attack targets are usually ordinary terminal devices, which do not have the function and authority to pass the physical layer information to the higher layer for the detection or other application purposes. Even though end devices can do that, we will not recommend doing this because such information exposures the users' privacy and may lead to other security concerns. \textbf{Instead, it is feasible to use some nearby special-purpose equipment within the attack range that is disconnected from the victims' network and provides channel characteristics to alarm \T{SigOver}.} Our paper looks into the channel estimated by these so-called \textbf{guard UEs} and their reflection on the existence of attackers (see Section \ref{Attacker's impact on Channel}).There remain two challenges to ensuring the practicability of the framework in deployment: the number of guards and the location. Although we failed to give specific answers, our evaluation indicates that \T{SigDetect} can identify \T{SigOver} in the scene of walking speed without degrading the overall accuracy (see Section \ref{multi}), assuring detection's deployment flexibility and convenience.

\iffalse
We can improve detector performance by using more informative metrics than RSSI, e.g., measurements of the wireless channel already made by UEs for equalization, the channel frequency response. Previous studies (see Section~\ref{reference}) have relied on the use of various channel features to detect spoofing attacks. These methods leverage the effects of attacks on channel characteristics as measured by victims of the attacks. We seek to improve detector performance by also considering the effects of such attacks on the channel measurements made by other endpoints within range of the attacker (other COTS handsets or specialty hardware deployed specifically for detecting such attacks). The benefits of using this collaborative approach are two-fold: (i) these \textbf{guard UEs} may detect such attacks and alert the network that they are taking place despite the fact that they are not the target of the attacks, while the target of a successful attack may not be able to notify the network; and (ii) detector performance increases with the number of guard UEs.
\fi

%Two challenges that remain for the practicability of the detector deployment are how many guards and where to deploy them. Although we failed to give specific answers, our evaluation indicates that \T{SigDetect} can identify \T{SigOver} in the scene of walking speed without significantly deteriorating the overall accuracy (see Section~\ref{multi}), which expands the detection's deployment flexibility and convenience.
 %These detectors can be deployed through SDR listening without attaching to the same network. Alternatively, they can be mobile phones or other end devices act as a crowd-solution, assuming that the communication chip manufacturer can provide the access authority or an interface to specific channel characteristics (discussed in Section \ref{deployment}). 

%So far, we have determined the basic framework of SigDect: collecting channel features on special-purpose guard equipment to detect \T{SigOver}. Next is the algorithm for \T{SigDetect}. \T{SigDetect} is essentially a binary classifier that aims to detect channel anomalies. The adversary usually is the cause of the channel anomalies because the communication on the commercial frequency bands requires a license to avoid in-band signal interference. It is also possible that in-band noise occurs in the intersection area of two cells with the same frequency band, which is relatively rare. In real life, we cannot traverse all channel changes caused by attacks in advance to make the classifier learn thoroughly, so supervised learning cannot be applied to this scenario. However,  normal samples are available, assuming we deploy detection equipment before or outside the attack range. This way, a binary classifier becomes a one-class classification problem to find out the outlier, which Novelty Detection can solve.


%The adversary usually is the cause of the channel anomalies, either a \T{SigOver} attacker or some other impersonate attacker because the communication on the commercial frequency bands requires a license to avoid in-band signal interference. It is also possible that in-band noise occurs in the intersection area of two cells with the same frequency band, though relatively rare. So \T{SigDetect} is a detection not only for SigOver but also for any impersonate attack. It is essentially a binary classifier that aims to detect channel anomalies. In real life, we cannot make the classifier learn by channel changes under attack in advance as supervised learning. Assuming we deploy detection equipment before or outside the attack range, only normal samples are available.  Thus, a binary classifier turns into a one-class classification for identifying outliers, a problem for Novelty Detection.

%We reproduce a SigOver paging attack in an indoor space, conduct multiple experiments to evaluate channel features chosen for \T{SigDetect}, compare \T{SigDetect} with an RSSI-based method, simulate the distributed detection framework, and verify performance in a mobile scenario. From our results, the features selected by \T{SigDetect} are highly robust when the attack signal is weak and are very effective for identifying signal injection attacks with high concealment - SigOver. Compared with the random interference generated by the environmental factors on the signal strength, the channel changes more slowly and smoothly. Thus \T{SigDetect} achieves meager false positive and false negative rates.

\iffalse
To evaluate out approach, we reproduce a paging attack in an indoor space. Compared SigDetect features with features used in other previous mentioned approaches and verify performance in a mobility scenario. From our results, \T{SigDetect} quickly recovers its detection ability to an AUC score of 0.745 from 0.5, while RSSI-based detection keeps the same at 0.5 after we increase attack transmission gain of 2, which indicates \T{SigDetect} does not rely on the signal strength and is very effective for identifying stealthy signal injection attacks.

Finally, we discuss the possibility of SigOver in 5G networks showing the need for \T{SigDetect} and \T{SigDetect}'s potential to be a crowd-sourced solution given the trend for manufacturers to open the interface and provide access to more physical layer channel information.

Our major contributions are summarized as follows:

\begin{itemize}[noitemsep,topsep=0pt]
\item \textbf{Collaborative framework, community contribution:} We propose \T{SigDetect}, a practical collaborative framework that detects signal injection attacks in cellular networks, e.g., SigOver like attacks, by monitoring changes in channel frequency response estimated on nearby special-purpose detectors or end-devices (including victims under non-DoS attacks).
\item \textbf{Advantages in varying real-world environments:} We analyze the challenge of using RSSI to detect signal injection attacks like SigOver in real-world LTE networks, investigate representative features from channel frequency response measurements on guard UEs, and compare the two methods.
\item \textbf{Evaluation and mobility:} We implement \T{SigDetect} and evaluate its effectiveness. Also, we demonstrate \T{SigDetect}'s deployment flexibility via mobility scenario results.
\end{itemize}
\fi

\iffalse
Technology is catching up so fast, and 5G has rapidly rolled out globally in just a few years. 
Whether the new 5G protocol fills in previous security gaps, 
and whether attacks like \T{SigOver} can still threaten cellular communications? 
The answerer is negative. 
Though equipped with enhanced security features, 
new standards cannot address security issues entirely. 
First, SUPI concealed by elliptic curve integrated encryption schemes (ECIESs) 
with a home operator public key is used to identify UEs, 
and frequently updated random temporary identifiers can exclude linking and tracking UEs, 
which seems to invalidate \T{SigOver} from attacking specific targets. 
However, researchers found that 
modifying the unencrypted UE capacity message can downgrade the devices to an older network,
which implies, everything, 
including attacks, can be restored to a 4G or even lower generation state.
On the other hand, 5G SA is still in an initial stage,  
4G and 5G NSA are still the current central deployment, 
 the reliance on IMSI, and the attack and defense around IMSI will not end prematurely. 
Second, the fundamental broadcast security issue remains. 
Even though many believe in the necessity of a Public-key infrastructure (PKI) 
based authentication mechanism for broadcasting messages 
from base stations \cite{hu2019systematic,hussain2019insecure}, the 5G protocol still retains keeping them in plaintext. 
However, an approach from Ericsson \cite{nakarmi2021murat}, 
adopted in the 5G security specification, 3GPP TS 33.501, 
proposed a UE-assisted network-based false base station detection method using measurement reports. 
Such a framework is efficient and works as a wonder for the Fake Base Station attack. 

based on this frame work, \T{SigDetect}  exploits enriching current measurement report with indicators
for signal injection detection and also explore the contribution from machine learning in detecting attacks.
Spoofing detection is a cliche but still hot because of the appearance of new attacks. 
Researches primarily work around RSSI, channel information, 
and random encryption, combined with fingerprint, overhead authentication, 
centralized or distributed methods. 
The central idea is to catch fixed and critical differences reflected 
before and after a spoofing attack.


Nevertheless, it is not a threat for signal injection attacks 
because they will not even be recorded as a potential cell in the measurement report. 
Within the existing framework, 
we need to think about how to modify the framework to respond to signal injection attacks.

fingerprint/authentication methods

 Now the research of physical-layer
authentication/key generation/encryption is still in the
primary stage and cannot integrate with the latest
physical-layer transmission techniques to fully develop the
characteristics of the physical-layer resources. With the
development of technology, physical-layer security
techniques would be applied more in mobile communication
system

\fi
 


